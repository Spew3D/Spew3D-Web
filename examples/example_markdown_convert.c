/* Copyright (c) 2023, ellie/@ell1e & Spew3D Web Team (see AUTHORS.md).

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Alternatively, at your option, this file is offered under the Apache 2
license, see accompanied LICENSE.md.
*/

/// WELCOME TO THE COMPLEX MARKDOWN EXAMPLEE.
/// This is a small example cool to convert markdown to HTML, which
/// also shows how to implement custom link conversion options!
/// For example, often you may want to convert relative links ending
/// in ".md" to links ending in ".html" if you save the result as HTML.
/// This tool has an option for that: --replace-rel-link-ext md html

#define SPEW3D_IMPLEMENTATION  // Only if not already in another file!
#define SPEW3D_OPTION_DISABLE_SDL  // Optional, drops graphical stuff.
#include <spew3d.h>
#define SPEW3DWEB_IMPLEMENTATION  // Only if not already in another file!
#include <spew3dweb.h>
#include <stdio.h>
#include <string.h>

// Some options that can be controlled via command line arguments:
int replace_url_start_count = 0;
static char **replace_url_start_old = NULL;
static char **replace_url_start_new = NULL;
static const char *replace_rel_ext_old = NULL;
static const char *replace_rel_ext_new = NULL;
static const char *replace_ext_old = NULL;
static const char *replace_ext_new = NULL;

// This little helper function lets us change the file extension
// of URIs in our converted markdown, if the globals to request
// such a change were set:
char *our_little_uri_transform_helper(
        const char *uristr, void *userdata
        ) {
    char *result = strdup(uristr);
    if (!result)
        return NULL;

    {  // This implements --replace-link-start
        int i = 0;
        while (i < replace_url_start_count) {
            if (strlen(result) >= strlen(replace_url_start_old[i]) &&
                    memcmp(result, replace_url_start_old[i],
                        strlen(replace_url_start_old[i])) == 0) {
                char *newresult = malloc(strlen(result) +
                    strlen(replace_url_start_new[i]) + 1);
                if (!newresult)
                    return NULL;
                memcpy(newresult, replace_url_start_new[i],
                    strlen(replace_url_start_new[i]));
                int skip = strlen(replace_url_start_old[i]);
                memcpy(newresult + strlen(replace_url_start_new[i]),
                    result + skip, strlen(result) - skip + 1);
                free(result);
                result = newresult;
                break;
            }
            i += 1;
        }
    }

    if (replace_rel_ext_new &&  // This implements --replace-rel-link-ext
            strstr(uristr, "://") == NULL) {
        if (replace_rel_ext_old &&
                s3d_uri_HasFileExtension(result, replace_rel_ext_old)
                ) {
            char *newuristr = (
                s3d_uri_SetFileExtension(result, replace_rel_ext_new)
            );
            free(result);
            return newuristr;
        }
    }

    if (replace_ext_new) {  // This implements --replace-link-ext
        if (replace_ext_old &&
                s3d_uri_HasFileExtension(result, replace_ext_old)
                ) {
            char *newuristr = (
                s3d_uri_SetFileExtension(result, replace_ext_new)
            );
            free(result);
            return newuristr;
        }
    }

    return result;
}

int main(int argc, const char **argv) {
    const char *filepath = NULL;

    // Parse some command line options we want to support:
    int i = 1;
    while (i < argc) {
        if (strcmp(argv[i], "--") == 0) {
            if (i + 1 < argc)
                filepath = argv[i + 1];
            break;
        } else if (strcmp(argv[i], "--help") == 0) {
            printf("A small example tool for markdown to HTML!\n");
            return 0;
        } else if (strcmp(argv[i], "--replace-link-start") == 0) {
            if (i + 2 > argc)
                break;
            replace_url_start_count++;
            replace_url_start_old = realloc(
                replace_url_start_old, sizeof(*replace_url_start_old) *
                    (replace_url_start_count));
            replace_url_start_new = realloc(
                replace_url_start_new, sizeof(*replace_url_start_new) *
                    (replace_url_start_count));
            if (!replace_url_start_old || !replace_url_start_new) {
                free(replace_url_start_old);
                free(replace_url_start_new);
                printf("Memory alloc failure.");
                return 1;
            }
            replace_url_start_old[replace_url_start_count - 1] = argv[i + 1];
            replace_url_start_new[replace_url_start_count - 1] = argv[i + 2];
            i += 3;
            continue;
        } else if (strcmp(argv[i], "--replace-rel-link-ext") == 0) {
            if (i + 2 < argc) {
                replace_rel_ext_old = argv[i + 1];
                replace_rel_ext_new = argv[i + 2];
            }
            i += 3;
            continue;
        } else if (strcmp(argv[i], "--replace-link-ext") == 0) {
            if (i + 2 < argc) {
                replace_ext_old = argv[i + 1];
                replace_ext_new = argv[i + 2];
            }
            i += 3;
            continue;
        } else if (strcmp(argv[i], "--version") == 0) {
            printf("example_markdown_convert.c v0.1\n");
            return 0;
        } else if (filepath == NULL &&
                argv[i][0] != '-') {
            filepath = argv[i];
        } else {
            fprintf(stderr, "warning: unrecognized "
                "argument: %s\n", argv[i]);
            return 1;
        }
        i += 1;
    }
    if (!filepath) {
        fprintf(stderr, "error: please specify a file path as a "
            "command line argument!\n");
        return 1;
    }

    // Open the original markdown file:
    FILE *f = fopen(filepath, "rb");
    if (!f) {
        fprintf(stderr, "error: failed to open file: %s\n", filepath);
        return 1;
    }

    // Read it in chunks and convert it:
    while (1) {
        size_t chunklen = 0;
        char *chunk = spew3dweb_markdown_GetIChunkFromDiskFile(
            f, 10 * 1024,
            &chunklen
        );
        if (chunk && chunklen == 0) {  // End of file.
            free(chunk);
            break;
        }
        s3dw_markdown_tohtmloptions options = {0};
        options.uritransform_callback = our_little_uri_transform_helper;
        char *html = (chunk ? spew3dweb_markdown_ToHTMLEx(
            chunk, &options, NULL) : NULL);
        if (!chunk || !html) {
            free(chunk);
            free(html);
            fprintf(stderr, "error: I/O or out of memory error\n");
            return 1;
        }
        printf("%s\n", html);
        free(chunk);
        free(html);
    }
    fclose(f);
    return 0;
}

